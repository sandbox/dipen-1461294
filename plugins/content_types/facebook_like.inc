<?php
/**
 * @file
 * Plugin definition for "Facebook like".
 *
 * Copyright (c) 2010-2011 Board of Trustees, Leland Stanford Jr. University
 * This software is open-source licensed under the GNU Public License Version 2 or later
 * The full license is available in the LICENSE.TXT file at the root of this repository
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Facebook Like'),
  'description' => t('Share the content using Facebook like.'),
  'single' => TRUE,
  'content_types' => array('social_plugins_fb_like'),
  'render callback' => 'ctools_social_facebook_like_content_type_render',
  'icon' => 'facebook.ico',
  'category' => array(t('Social Plugins')),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function ctools_social_facebook_like_content_type_render($subtype, $conf, $args, $context) {
  $path = ctools_social_plugins_get_current_path();
  $block = new stdClass();
  $block->content = '<div class="hover-block"><iframe src="http://www.facebook.com/plugins/like.php?href=' . $path . '&amp;send=false&amp;
  layout=button_count&amp;width=100&amp;show_faces=false&amp;action=like&amp;
  colorscheme=light&amp;font&amp;height=21&amp;appId=212524242126581" scrolling="no" 
  frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" 
  allowTransparency="true"></iframe></div>';

  return $block;
}
