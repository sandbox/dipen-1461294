<?php
/**
 * @file
 * "Twitter share" sample content type. It help to share content at Twitter.
 *
 * Copyright (c) 2010-2011 Board of Trustees, Leland Stanford Jr. University
 * This software is open-source licensed under the GNU Public License Version 2 or later
 * The full license is available in the LICENSE.TXT file at the root of this repository
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Twitter share'),
  'description' => t('Share the content at Twitter.com.'),
  'single' => TRUE,
  'content_types' => array('social_plugin_twitter'),
  'render callback' => 'ctools_social_plugins_twitter_content_type_render',
  'defaults' => array(),
  'icon' => 'twitter.ico',
  'category' => array(t('Social Plugins')),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function ctools_social_plugins_twitter_content_type_render($subtype, $conf, $args, $context) {
  $path = ctools_social_plugins_get_current_path();
  $block = new stdClass();
  $block->content = '
	  <div class="hover-block">
  		 <a href="https://twitter.com/share" data-url="' . $path . '" class="twitter-share-button" data-lang="en">Tweet</a>
  			<script>!function(d,s,id){
   				 var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){
     			js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);
    		}
  			}(document,"script","twitter-wjs");</script>
		</div>';
  return $block;
}
