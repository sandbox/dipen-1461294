<?php
/**
 * @file
 * "Google plus share" sample content type. It help to share content at Google plus.
 *
 * Copyright (c) 2010-2011 Board of Trustees, Leland Stanford Jr. University
 * This software is open-source licensed under the GNU Public License Version 2 or later
 * The full license is available in the LICENSE.TXT file at the root of this repository
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Google plus share'),
  'description' => t('Share the content using Google plus.'),
  'single' => TRUE,
  'content_types' => array('social_plugin_google_plus'),
  'render callback' => 'ctools_social_plugins_google_plus_content_type_render',
  'defaults' => array(),
  'icon' => 'google_plus.ico',
  'category' => array(t('Social Plugins')),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function ctools_social_plugins_google_plus_content_type_render($subtype, $conf, $args, $context) {
  $path = ctools_social_plugins_get_current_path();
  $block = new stdClass();
  $block->content = '
	  <div class="hover-block">
		  <!-- Place this tag where you want the +1 button to render -->
		  <g:plusone href="' . $path . '" size="medium"></g:plusone>
		
		  <!-- Place this render call where appropriate -->
		  <script type="text/javascript">
		    (function() {
		      var po = document.createElement(\'script\'); po.type = \'text/javascript\'; po.async = true;
		      po.src = \'https://apis.google.com/js/plusone.js\';
		      var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(po, s);
		    })();
		  </script>
	  </div>'; 
  return $block;
}
